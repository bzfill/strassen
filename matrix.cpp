#include "matrix.hpp"

MatrixCube::~MatrixCube() { clean(); }

void MatrixCube::gen(int seed, size_t size) {
  std::default_random_engine e;
  e.seed(seed);
  std::uniform_int_distribution<int> d(0, 100);
  reserve(size);
  for (int i = 0; i < size; i++)
    for (int j = 0; j < size; j++)
      setCell(i, j, d(e));
}

int MatrixCube::getCell(size_t row, size_t column) const {
  if (row > (size - 1) || column > (size - 1))
    throw std::out_of_range("exceeded matrix size");
  return *((data + row * size) + column);
}

void MatrixCube::setCell(size_t row, size_t column, int val) {
  if (row > (size - 1) || column > (size - 1))
    throw std::out_of_range("exceeded matrix size");
  *((data + row * size) + column) = val;
}

void MatrixCube::clean() {
  if (data != nullptr)
    delete data;
  data = nullptr;
  size = 0;
}

void MatrixCube::reserve(size_t size) {
  clean();
  data = new int[size * size];
  this->size = size;
}

size_t MatrixCube::getSize() const { return this->size; }

void MatrixCube::mult(const AbstractMatrix &mat) {
  if (this->getSize() != mat.getSize())
    throw std::invalid_argument("sizes of matrices must be equal");

  int *buf = new int[size * size];
  memset(buf, 0, size * size * sizeof(*data));
  auto buf_cell = [&buf, this](size_t row, size_t column) -> int & {
    return *((buf + row * this->size) + column);
  };

  for (int i = 0; i < size; i++)     // row of fist matrix
    for (int j = 0; j < size; j++)   // column of second matrix
      for (int k = 0; k < size; k++) // cells
        buf_cell(i, j) += this->getCell(i, k) * mat.getCell(k, j);
  auto size_save = this->size;
  clean();
  this->data = buf;
  this->size = size_save;
}

std::string MatrixCube::toString() const {
  std::string ret = "";
  if (!size)
    return ret;

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++)
      ret += std::to_string(this->getCell(i, j)) + " ";
    ret += "\n";
  }
  return ret;
}

MatrixStrassen::MatrixStrassen(const AbstractMatrix &mat) {
  reserve(mat.getSize());
  for (int i = 0; i < mat.getSize(); i++)
    for (int j = 0; j < mat.getSize(); j++)
      this->setCell(i, j, mat.getCell(i, j));
}

MatrixStrassen::MatrixStrassen(const MatrixStrassen &mat) {
  reserve(mat.getSize());
  for (int i = 0; i < mat.getSize(); i++)
    for (int j = 0; j < mat.getSize(); j++)
      this->setCell(i, j, mat.getCell(i, j));
}

MatrixStrassen::~MatrixStrassen() { clean(); }

void MatrixStrassen::gen(int seed, size_t size) {
  std::default_random_engine e;
  e.seed(seed);
  std::uniform_int_distribution<int> d(0, 100);
  reserve(size);
  for (int i = 0; i < size; i++)
    for (int j = 0; j < size; j++)
      setCell(i, j, d(e));
}

bool MatrixStrassen::isVirtual() const {
  if (attached_matrix == nullptr)
    return false;
  else
    return true;
}

int MatrixStrassen::getCell(size_t row, size_t column) const {
  if (row > (size - 1) || column > (size - 1))
    throw std::out_of_range("exceeded matrix size");

  if (this->isVirtual())
    if ((row + row_offset >= attached_matrix->getSize()) ||
        (column + column_offset >= attached_matrix->size))
      return 0;
    else
      return *(attached_matrix->data +
               (row + row_offset) * attached_matrix->size +
               (column + column_offset));
  else
    return *((data + row * size) + column);
}

void MatrixStrassen::setCell(size_t row, size_t column, int val) {
  if (row > (size - 1) || column > (size - 1))
    throw std::out_of_range("exceeded matrix size");

  if (isVirtual())
    throw std::runtime_error("can't change attached matrix");

  *((data + row * size) + column) = val;
}

void MatrixStrassen::clean() {
  if (this->isVirtual()) {
    attached_matrix = nullptr;
    column_offset = 0;
    row_offset = 0;
  } else
    delete data;

  data = nullptr;
  size = 0;
}

void MatrixStrassen::reserve(size_t size, bool zfill) {
  clean();
  data = new int[size * size];
  this->size = size;
  if (zfill)
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
        setCell(i, j, 0);
}

size_t MatrixStrassen::getSize() const { return this->size; }

void MatrixStrassen::multCube(const MatrixStrassen &mat) {
  if (this->getSize() != mat.getSize())
    throw std::invalid_argument("sizes of matrices must be equal");

  int *buf = new int[size * size];
  memset(buf, 0, size * size * sizeof(*data));
  auto bufCell = [&buf, this](size_t row, size_t column) -> int & {
    return *((buf + row * this->size) + column);
  };

  for (int i = 0; i < size; i++)     // row of fist matrix
    for (int j = 0; j < size; j++)   // column of second matrix
      for (int k = 0; k < size; k++) // cells
        bufCell(i, j) +=
            *((this->data + i * this->size) + k) * mat.getCell(k, j);

  auto size_save = this->size;
  clean();
  this->data = buf;
  this->size = size_save;
}

MatrixStrassen MatrixStrassen::multStrassen(const MatrixStrassen &l,
                                            const MatrixStrassen &r) {

  if (l.size <= LEAF_SIZE) {
    MatrixStrassen ret = l;
    ret.multCube(r);
    return ret;
  }

  MatrixStrassen A_11, A_12, A_21, A_22;
  MatrixStrassen B_11, B_12, B_21, B_22;
  int half_size = l.size / 2 + l.size % 2;

  A_11.attachMatrix(l, 0, 0, half_size);
  A_12.attachMatrix(l, 0, half_size, half_size);
  A_21.attachMatrix(l, half_size, 0, half_size);
  A_22.attachMatrix(l, half_size, half_size, half_size);

  B_11.attachMatrix(r, 0, 0, half_size);
  B_12.attachMatrix(r, 0, half_size, half_size);
  B_21.attachMatrix(r, half_size, 0, half_size);
  B_22.attachMatrix(r, half_size, half_size, half_size);

  auto M_1 = multStrassen(A_11 + A_22, B_11 + B_22);
  auto M_2 = multStrassen(A_21 + A_22, B_11);
  auto M_3 = multStrassen(A_11, B_12 - B_22);
  auto M_4 = multStrassen(A_22, B_21 - B_11);
  auto M_5 = multStrassen(A_11 + A_12, B_22);
  auto M_6 = multStrassen(A_21 - A_11, B_11 + B_12);
  auto M_7 = multStrassen(A_12 - A_22, B_21 + B_22);

  auto C_11 = M_1; // + M_4 - M_5 + M_7;
  C_11 += M_4;
  C_11 -= M_5;
  C_11 += M_7;

  auto C_12 = M_3; // + M_5;
  C_12 += M_5;

  auto C_21 = M_2; // + M_4;
  C_21 += M_4;

  auto C_22 = M_1; // - M_2 + M_3 + M_6;
  C_22 -= M_2;
  C_22 += M_3;
  C_22 += M_6;

  // concatination
  MatrixStrassen ret;
  ret.reserve(l.size);
  int row_offsets[] = {0, 0, half_size, half_size};
  int column_offsets[] = {0, half_size, 0, half_size};
  int row_cut[] = {0, 0, static_cast<int>(l.size % 2),
                   static_cast<int>(l.size % 2)};

  int column_cut[] = {0, static_cast<int>(l.size % 2), 0,
                      static_cast<int>(l.size % 2)};
  int i = 0;
  for (auto &c : {C_11, C_12, C_21, C_22}) {
    for (int j = 0; j < c.size - row_cut[i]; j++)      // row
      for (int k = 0; k < c.size - column_cut[i]; k++) // column
        ret.setCell(row_offsets[i] + j, column_offsets[i] + k, c.getCell(j, k));
    i++;
  }

  return ret;
}

void MatrixStrassen::mult(const AbstractMatrix &mat) {
  if (this->getSize() != mat.getSize())
    throw std::invalid_argument("sizes are different");

  MatrixStrassen m = mat;
  *this = multStrassen(*this, m);
}

std::string MatrixStrassen::toString() const {
  std::string ret = "";
  if (!size)
    return ret;

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++)
      ret += std::to_string(this->getCell(i, j)) + " ";
    ret += "\n";
  }
  return ret;
}

MatrixStrassen &MatrixStrassen::operator=(MatrixStrassen rhs) {
  auto data_buf = rhs.data;
  rhs.data = this->data;
  this->data = data_buf;
  this->size = rhs.size;
  attached_matrix = nullptr;
  row_offset = 0;
  column_offset = 0;
  return *this;
}

MatrixStrassen &MatrixStrassen::operator+=(const MatrixStrassen &rhs) {
  if (this->getSize() != rhs.getSize())
    throw std::invalid_argument("sizes are different");

  for (int i = 0; i < rhs.getSize(); i++)
    for (int j = 0; j < rhs.getSize(); j++)
      setCell(i, j, this->getCell(i, j) + rhs.getCell(i, j));

  return *this;
}

MatrixStrassen &MatrixStrassen::operator-=(const MatrixStrassen &rhs) {
  if (this->getSize() != rhs.getSize())
    throw std::invalid_argument("sizes are different");

  for (int i = 0; i < rhs.getSize(); i++)
    for (int j = 0; j < rhs.getSize(); j++)
      setCell(i, j, this->getCell(i, j) - rhs.getCell(i, j));

  return *this;
}

MatrixStrassen MatrixStrassen::operator+(const MatrixStrassen &rhs) {
  MatrixStrassen ret = *this;
  ret += rhs;
  return ret;
}

MatrixStrassen MatrixStrassen::operator-(const MatrixStrassen &rhs) {
  MatrixStrassen ret = *this;
  ret -= rhs;
  return ret;
}

void MatrixStrassen::attachMatrix(const MatrixStrassen &mat, size_t row_offset,
                                  size_t column_offset, size_t size) {
  clean();
  if (row_offset >= mat.getSize() || column_offset >= mat.getSize())
    throw std::invalid_argument("exceeded matrix size");

  if (mat.isVirtual()) {
    this->attached_matrix = mat.attached_matrix;
    this->row_offset = row_offset + mat.row_offset;
    this->column_offset = column_offset + mat.column_offset;
  } else {
    this->attached_matrix = &mat;
    this->row_offset = row_offset;
    this->column_offset = column_offset;
  }

  this->size = size;
}
