#include "matrix.hpp"
#include <chrono>
#include <iostream>

void matrixTest(AbstractMatrix &mat, AbstractMatrix &mat2) {
  auto start = std::chrono::high_resolution_clock::now();
  mat.mult(mat2);
  auto finish = std::chrono::high_resolution_clock::now();
  std::cout << "result: "
            << std::chrono::duration_cast<std::chrono::microseconds>(finish -
                                                                     start)
                   .count()
            << std::endl;
}

int main() {
  constexpr unsigned int s = 1000;
  MatrixStrassen a;
  MatrixStrassen b;

  a.gen(13, s);
  b.gen(20, s);

  MatrixCube a2;

  a2.gen(13, s);

  std::cout << a.getCell(10, 10) << "\n";
  std::cout << a2.getCell(10, 10) << "\n";

  matrixTest(a2, b);
  matrixTest(a, b);

  std::cout << a2.getCell(10, 10) << "\n";
  std::cout << a.getCell(10, 10) << "\n";

  return 0;
}
