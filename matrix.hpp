#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstdlib>
#include <iostream>
#include <random>
#include <string>

class AbstractMatrix {
public:
  virtual ~AbstractMatrix(){};
  virtual void mult(const AbstractMatrix &mat) = 0;
  virtual int getCell(size_t row, size_t column) const = 0;
  virtual void setCell(size_t row, size_t column, int val) = 0;
  virtual size_t getSize() const = 0;
  virtual std::string toString() const = 0;
};

class MatrixCube : public AbstractMatrix {
public:
  MatrixCube() = default;
  ~MatrixCube();

  void gen(int seed, size_t size);
  int getCell(size_t i, size_t j) const final;
  void setCell(size_t i, size_t j, int val) final;
  size_t getSize() const final;
  void mult(const AbstractMatrix &mat);
  std::string toString() const;

private:
  void clean();
  void reserve(size_t size);
  int *data = nullptr;
  size_t size = 0;
};

class MatrixStrassen : public AbstractMatrix {
public:
  MatrixStrassen() = default;
  MatrixStrassen(const AbstractMatrix &mat);
  MatrixStrassen(const MatrixStrassen &mat);
  ~MatrixStrassen();

  void gen(int seed, size_t size);
  int getCell(size_t row, size_t column) const final;
  void setCell(size_t row, size_t column, int val) final;
  size_t getSize() const final;
  void mult(const AbstractMatrix &mat);
  MatrixStrassen &operator=(MatrixStrassen rhs);
  MatrixStrassen &operator+=(const MatrixStrassen &rhs);
  MatrixStrassen &operator-=(const MatrixStrassen &rhs);
  MatrixStrassen operator+(const MatrixStrassen &rhs);
  MatrixStrassen operator-(const MatrixStrassen &rhs);
  bool isVirtual() const;
  std::string toString() const;

private:
  static constexpr unsigned int LEAF_SIZE = 64;
  void clean();
  void reserve(size_t size, bool zfill = false);
  MatrixStrassen multStrassen(const MatrixStrassen &lhs,
                              const MatrixStrassen &rhs);
  void multCube(const MatrixStrassen &mat);
  void attachMatrix(const MatrixStrassen &mat, size_t row_offset,
                    size_t column_offset, size_t size);
  int *data = nullptr;
  size_t size = 0;

  const MatrixStrassen *attached_matrix = nullptr;
  size_t row_offset = 0;
  size_t column_offset = 0;
};

#endif
